108	Who uses a supercomputer anyway?	who-uses-a-supercomputer-anyway		<p>Who uses a supercomputer anyway?</p><p>This is a presentation about Super Computers, I found on the internet. I found it quite useful for those hungry minds, who are just starting to learn about Super Computing. I got a permission to re-publish this presentation, from the original author, Mr. Jim Greer from Tyndall National Institute, Ireland.</p><p>The presentation is provided, on "as is" basis. I have converted it to PDF and ODP formats for your convenience.</p><p><a href="images/howtos/thurs_1620-1700_main_jim_greer.odp" target="_blank" title="JimGreer - Who uses a Super Computer anyway? [ODP] [4.3 MB]"> \	\	\	\	\	JimGreer - Who uses a Super Computer anyway? [ODP] [4.3 MB]</a></p><p><a href="images/howtos/thurs_1620-1700_main_jim_greer.ppt" target="_blank" title="JimGreer - Who uses a Super Computer anyway? [PPT] [4.3 MB]"> \	\	\	\	\	JimGreer - Who uses a Super Computer anyway? [PPT] [4.35 MB]</a></p><p>&nbsp;</p><p>Thank you Mr. Jim Greer. </p><p>&nbsp;</p><p>Regards,</p><p>Kamran</p><p>&nbsp;</p>		1	11	0	39	2009-06-14 08:19:26	64		2009-06-14 08:41:43	63	0	0000-00-00 00:00:00	2009-06-14 08:19:26	0000-00-00 00:00:00			show_title=\
link_titles=\
show_intro=\
show_section=\
link_section=\
show_category=\
link_category=\
show_vote=\
show_author=\
show_create_date=\
show_modify_date=\
show_pdf_icon=\
show_print_icon=\
show_email_icon=\
language=\
keyref=\
readmore=	2	0	8			0	7986	robots=\
author=
