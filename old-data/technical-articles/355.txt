355	How to prepare for CEH exam?	how-to-prepare-for-ceh-exam		<p>In March 2012, I got certified under CEH v7 certification. (EC1-350).  It was indeed a tough exam. Since a lot of people are asking me on the  guide lines for clearing this exam, I decided to put them down for you.</p>\
<h2>Study Material:</h2>\
<h3>Books:</h3>\
<p>I studied about four books directly related to CEH exam. These are:</p>\
<ul>\
<li>CEH Certified Ethical Hacker Study Guide by Kimberly Graves ( ISBN-13: 978-0470525203 )</li>\
<li>CEH: Official Certified Ethical Hacker by Kimberly Graves ( ISBN-13: 978-0782144376 )</li>\
<li>Certified Ethical Hacker Exam Prep by Michael Gregg ( ISBN-13: 978-0789735317 )</li>\
<li>CEH Certified Ethical Hacker All-in-One Exam Guide by Matt Walker ( ISBN-13: 978-0071772297)</li>\
</ul>\
<p><br /></p>	In addition to the books mentioned above, I also studied some extra books:<p><br /></p>\
<ul>\
<li>Practical  Packet Analysis: Using Wireshark to Solve Real-World Network Problems  by Chris Sanders (ISBN-10: 1593272669 | ISBN-13: 978-1593272661)</li>\
<li>BackTrack 5 Wireless Penetration Testing Beginner's Guide by Vivek Ramachandran (ISBN-10: 1849515581 | ISBN-13: 978-1849515580)</li>\
<li>BackTrack  4: Assuring Security by Penetration Testing by Shakeel Ali , Tedi  Heriyanto (ISBN-10: 1849513945 | ISBN-13: 978-1849513944)</li>\
<li>Nmap Network Scanning: The Official Nmap Project Guide to Network Discovery and Security Scanning by (Gordon Fyodor Lyon) (ISBN-10 : 0-9799587-1-7 | ISBN-13 978-0-9799587-1-7 )</li>\
<li>Hacking Exposed (5th Edition) (ISBN: 007226081-5)</li>\
</ul>\
<p>Out  of the four CEH books, I liked the All-in-One Guide by Matt Walker, and  Exam Prep by Micheal Gregg. It must be noted that only the All-in-One  by Matt Walker is the book updated with CEH v7 content. I did not find  Kimberly's books any useful at all. You opinion and experience may  differ.</p>\
<p>The extra books mentioned above have been a real help. I  really recommend everyone to read them in addition to any&nbsp; CEH exam  guide you may be studying. Each book mentioned above took 10-15 days of  mine, including any practice needed to learn the tools and techniques. I  studied for 8-10 hours a day. That makes 80 - 120 days of book study.</p>\
<h3>Security tools:</h3>\
<p>Following  are few Linux Distributions, which I really like, and recommend to  everyone. They contain a lot of tools, which will help you practice  various concepts mentioned in books, and various websites on the  Internet.</p>\
<ul>\
<li>BackTrack Linux (<a title="BackTrack Linux" href="http://www.backtrack-linux.org/" mce_href="http://www.backtrack-linux.org/" data-mce-href="http://www.backtrack-linux.org/">http://www.backtrack-linux.org/</a>)</li>\
<li>Fedora Security Lab (<a title="Fedora Security Lab" href="http://spins.fedoraproject.org/security/" mce_href="http://spins.fedoraproject.org/security/" data-mce-href="http://spins.fedoraproject.org/security/">http://spins.fedoraproject.org/security/</a>)</li>\
<li>STD - Security Tools Distribution (<a title="Security Tools Distribution" href="http://s-t-d.org/" mce_href="http://s-t-d.org/" data-mce-href="http://s-t-d.org/">http://s-t-d.org/</a>)</li>\
</ul>\
<p><br /></p>\
<h2>Study plan:</h2>\
<h3>Step 1:</h3>\
<p>Study  each book (study guide /exam guide), chapter by chapter. Normally one  chapter a day. Attempt sample questions at the end of each chapter (of  each study / exam guide), and keep the record of your score. Also make a  list of your weak areas. (10 - 15 days per book).</p>\
<h3>Step 2:</h3>\
<p>Attempt the sample exam at the end of Gregg's Exam Prep book. Make a record of score and note weak areas. (2-3 hours, one day)</p>\
<h3>Step 3:</h3>\
<p>When you finish all the CEH specific study/exam guides, then study the side books listed above. (10 -15 days per book).</p>\
<h3>Step 4:</h3>\
<p>Watch  various videos on YouTube, etc, related to the topics you are weak in.  This step can be done in parallel to Step 1 and Step 2. This is also a  good way to kill boredom while studying. There are a few interesting  channels on YouTube, which explain some concepts in light ways. Hak5.org  is a website, which has it's youtube channel as well. The show may not  explain anything really in depth, but is a good source to touch up and  to know a lot of&nbsp; things, small and big alike. There are other channels  and people as well, who have put up good information on various  websites. I watched this show just for a change of taste / kill boredom.</p>\
<h3>Step 5:</h3>\
<p>Study  the All-in-One exam guide by Matt again, from first chapter to the  last, optionally taking any notes. Attempt the sample exam "quiz"  provided on the CD, which comes with this book. Record your score. (The  software only installs/runs on windows :( ) . ( 1 - 3 days, total).</p>\
<h3>Step 6:</h3>\
<p>Cover  your weak areas identified in Step 5, and then attempt the sample  "master exam" provided on the same CD which comes with Matt's All-in-One  exam guide. (The software only installs/runs on windows :( ) (4 hours, 1  day)</p>\
<h2>Helpful tools and tips:</h2>\
<p>Make sure you play a lot  with various security tools and penetration / hacking tools, for both  Linux and Windows. You must be very well versed with&nbsp; tcpdump,  wireshark, nmap, netcat, snort, etc. You must be absolutely clear on how  to capture packets, and how to extract information out of them. You  must be absolutely clear on various scan types and various flags,  including their binary and hexadecimal representation. You must be good  in programming in C and C++, etc.</p>\
<p><br /></p>\
<h2>Exam code, exam cost, course outline:</h2>\
<p>CEH  (v7) has exam code of EC1-350. Exam itself&nbsp; is 500 USD. If you did not  take official training from any of the CEH certified instructors /  institutes, and studied at your own, like me, then you have to submit an  "Eligibility Application Form" to EC-Council, with an additional fee of  100 USD. That makes a total of 600 USD, making it an expensive exam.</p>\
<p>EC-Council  has provided a CEH handbook, in PDF form,&nbsp; which can be downloaded, and  printed. It contains the complete course outline / exam blue-print. It  also contains all the application forms, etc, as well as FAQs. It is  available on EC-Council's website http://eccouncil.org.</p>\
<p><br /></p>\
<h2>Conclusion:</h2>\
<p>If  you follow the above study plan, it will take 3-5 months for you to  prepare for CEH exam. Remember, the above plan worked for me. And the  books I chose worked for me. By no means you should take it as granted,  that Matt's All-in-One exam guide, is all you need! (Even Matt's  All-in-One guide has errors in it). You need to have a lot of  experience, and rock-solid concepts&nbsp; in Information System Security  field.</p>\
<p>CEH exam is 4 hours+ computer based multiple choice exam .  Be patient during the exam. May success be with you. Ameen. Good Luck!</p>\
<p><br /></p>	1	11	0	39	2012-03-26 11:33:52	64		2012-12-01 07:48:15	63	0	0000-00-00 00:00:00	2012-03-26 11:33:52	0000-00-00 00:00:00			show_title=\
link_titles=\
show_intro=\
show_section=\
link_section=\
show_category=\
link_category=\
show_vote=\
show_author=\
show_create_date=\
show_modify_date=\
show_pdf_icon=\
show_print_icon=\
show_email_icon=\
language=\
keyref=\
readmore=	7	0	1	ceh, CEH, Certified Ethical Hacker, CEH exam, EC1350, EC1-350, How to prepare for CEH exam ?	This article explains, how you can prepare for the CEH (Certified Ethical Hacker) exam.	0	32158	robots=\
author=Muhammad Kamran Azeem
