73	Updating clamav from 0.93 to 0.94-1	updating-clamav-from-093-to-094-1		<p>A day ago, my mail server stopped receiving and sending mails. I was getting the famous "qq Temporary problem". My suspected clamav for this. Reason being, clamav had been sending me warning mails that the version is outdated and I should upgrade. Due to my business, and partly my laziness, I could not update it. Any way, to get it working, I had to upgrade to the latest version of ClamAV, which is 0.94-1, by the time of this writing.</p>\
<p>Here is how I did it.</p>\
<p>Updating clamav from 0.93 to 0.94-1</p>\
<p>Check the status of ClamAV:-</p>\
<p>[root@www ~]# service clamd status<br />ERROR: Clamav DB missing! Run 'freshclam --verbose' as root.<br />[root@www ~]#</p>\
<p>This is because the naming convention is also changed for the Virus Signature files.</p>\
<p> </p>\
<p>Downlaod clamav Src RPM from http://packages.sw.be/clamav/ .<br /><br />wget http://packages.sw.be/clamav/clamav-0.94.1-1.rf.src.rpm<br /><br />Rebuild it. (make sure you have sendmail-devel installed before you rebuilt it). DO NOT install sendmail. Just install sendmail-devel.<br /><br /><span style="font-family: courier new,courier;">rpmbuild --rebuild clamav-0.94.1-1.rf.src.rpm </span><br /><br />Stop clamd and fresh clam<br /><br /><br /><br /><span style="font-family: courier new,courier;">service clamd stop<br />service freshclam stop</span><br /><br /><br />Save a copy of old configs:-<br /><br /><span style="font-family: courier new,courier;">cd /etc/<br />mv clamd.conf clamd.conf.93<br />mv freshclam.conf freshclam.conf.93</span><br /><br /><br /><br />You will need to remove the older clamav packages. Other wise this newer version will not install, even if you try to Update it.<br /><br /><br /><span style="font-family: courier new,courier;">cd /usr/src/redhat/RPMS/i386/<br /><br />[root@www i386]# rpm -Uvh clamav-0.94-1.rf.i386.rpm clamav-db-0.94-1.rf.i386.rpm clamd-0.94-1.rf.i386.rpm clamav-devel-0.94-1.rf.i386.rpm<br />error: Failed dependencies:<br /> libclamav.so.4 is needed by (installed) clamav-server-0.93-2.i386<br /> libclamav.so.4(CLAMAV_PRIVATE) is needed by (installed) clamav-server-0.93-2.i386<br /> libclamav.so.4(CLAMAV_PUBLIC) is needed by (installed) clamav-server-0.93-2.i386<br /> clamav = 120:0.93-2 is needed by (installed) clamav-server-0.93-2.i386<br /><br /><br /><br />[root@www i386]# rpm -qa | grep -i clam<br />clamav-db-0.93-2<br />clamav-devel-0.93-2<br />clamav-server-0.93-2<br />clamav-0.93-2<br /></span><br /><br />Remove old packages first:-<br /><br /><span style="font-family: courier new,courier;">[root@www i386]# rpm -e clamav-db clamav-devel clamav-server clamav<br />warning: /etc/logrotate.d/freshclam saved as /etc/logrotate.d/freshclam.rpmsave<br />warning: /etc/logrotate.d/clamd saved as /etc/logrotate.d/clamd.rpmsave</span><br /><br /><br />Now install new packages:-<br /><br /><span style="font-family: courier new,courier;">[root@www i386]# rpm -Uvh clamav-0.94-1.rf.i386.rpm clamav-db-0.94-1.rf.i386.rpm clamd-0.94-1.rf.i386.rpm clamav-devel-0.94-1.rf.i386.rpm<br />Preparing...                ########################################### [100%]<br /> 1:clamav-db              ########################################### [ 25%]<br /> 2:clamav                 ########################################### [ 50%]<br /> 3:clamd                  ########################################### [ 75%]<br /> 4:clamav-devel           ########################################### [100%]<br />[root@www i386]# <br /></span><br /><br />Edit the config files to reflect the changes. The major change in 0.94 is that it stores it's database in /var/clamav , instead of /var/lib/clamav .  This was the stupid reason that my clamav 0.93 was not able to find the virus databases. Any way.<br /> Also the Temporary directory should be /tmp, instead of /var/tmp. Make sure to have user as qscand instead of clamav, both in clamav and freshclam installation.</p>\
<p><br /><br /><span style="font-family: courier new,courier;">chown qscand:qscand /var/log/clamav -R<br />chown qscand:qscand /var/clamav -R<br />chown qscand:qscand /var/run/clamav -R</span></p>\
<p> </p>\
<p><span style="font-family: courier new,courier;">vi /etc/logrotate.d/clamd<br />#<br /># Rotate Clam AV daemon log file<br />#<br /><br />/var/log/clamav/clamd.log {<br />missingok<br />nocompress<br />create 640 qscand qscand<br />postrotate<br />/bin/kill -HUP `cat /var/run/clamav/clamd.pid 2&gt; /dev/null` 2&gt; /dev/null || true<br />endscript<br />}<br /><br /><br />vi /etc/logrotate.d/freshclam<br /><br />/var/log/clamav/freshclam.log {<br /> missingok<br /> notifempty<br /> create 644 qscand qscand<br />}</span></p>\
<p> </p>\
<p><span style="font-family: courier new,courier;">[root@www x86_64]# service clamd restart<br />Stopping Clam AntiVirus Daemon:                            [  OK  ]<br />Starting Clam AntiVirus Daemon: LibClamAV Warning: **************************************************<br />LibClamAV Warning: ***  The virus database is older than 7 days!  ***<br />LibClamAV Warning: ***   Please update it as soon as possible.    ***<br />LibClamAV Warning: **************************************************<br /> [  OK  ]<br /></span></p>\
<p>Run freshclam to update the Virus database:-</p>\
<p><span style="font-family: courier new,courier;">[root@www x86_64]# freshclam<br />ClamAV update process started at Fri Nov 14 12:35:07 2008<br />main.cvd is up to date (version: 49, sigs: 437972, f-level: 35, builder: sven)<br />WARNING: getfile: daily-8543.cdiff not found on remote server (IP: 64.142.100.50)<br />WARNING: getpatch: Can't download daily-8543.cdiff from db.local.clamav.net<br />WARNING: getfile: daily-8543.cdiff not found on remote server (IP: 64.142.100.50)<br />WARNING: getpatch: Can't download daily-8543.cdiff from db.local.clamav.net<br />WARNING: getfile: daily-8543.cdiff not found on remote server (IP: 64.142.100.50)<br />WARNING: getpatch: Can't download daily-8543.cdiff from db.local.clamav.net<br />WARNING: Incremental update failed, trying to download daily.cvd<br />Downloading daily.cvd [100%]<br />daily.cvd updated (version: 8631, sigs: 26049, f-level: 35, builder: ccordes)<br />Database updated (464021 signatures) from db.local.clamav.net (IP: 64.142.100.50)<br />Clamd successfully notified about the update.<br /></span></p>\
<p> </p>\
<p>Restart clamd :-</p>\
<p><span style="font-family: courier new,courier;">[root@www x86_64]# service clamd restart<br />Stopping Clam AntiVirus Daemon:                            [  OK  ]<br />Starting Clam AntiVirus Daemon:                            [  OK  ]<br />[root@www x86_64]# </span></p>\
<p><span style="font-family: courier new,courier;"> </span><br />For some reason freshclam service is removed from 0.94. This means, back to old method. Crontab:-<br /><br /><span style="font-family: courier new,courier;"># crontab -e<br />0 1 * * * /usr/local/bin/setuidgid qscand /var/qmail/bin/qmail-scanner-queue.pl -z<br />0 1 * * * /usr/local/bin/setuidgid qscand /var/qmail/bin/qmail-scanner-queue.pl -g<br />25 2 * * * /usr/bin/freshclam --quiet -l /var/log/clamav/freshclam.log<br /></span><br /><br /> Alhumdulillah.</p>\
<p>Now you may want to restart qmail, by:-</p>\
<p><span style="font-family: courier new,courier;"> qmailctl stop</span></p>\
<p><span style="font-family: courier new,courier;">qmailctl start</span></p>\
<p><span style="font-family: courier new,courier;">qmailctl stat</span></p>\
<p> </p>\
<p>Make sure you issue the following two commands as well. They are (should be) part of your crontab, by the way.</p>\
<p><span style="font-family: courier new,courier;">/usr/local/bin/setuidgid qscand /var/qmail/bin/qmail-scanner-queue.pl -z<br />/usr/local/bin/setuidgid qscand /var/qmail/bin/qmail-scanner-queue.pl -g </span></p>\
<p>Your mail server now has latest version of clamav. Congratulations.</p>\
<p>Alhumdulillah.</p>\
<br />\
<p> </p>\
<p> </p>		1	11	0	34	2008-11-13 15:49:55	64		2010-12-25 18:58:38	63	0	0000-00-00 00:00:00	2008-11-13 15:49:55	0000-00-00 00:00:00			show_title=\
link_titles=\
show_intro=\
show_section=\
link_section=\
show_category=\
link_category=\
show_vote=\
show_author=\
show_create_date=\
show_modify_date=\
show_pdf_icon=\
show_print_icon=\
show_email_icon=\
language=\
keyref=\
readmore=	5	0	64			0	5741	robots=\
author=
