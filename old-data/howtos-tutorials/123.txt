123	AutoResponder and Courier MailDrop for Qmail ...	autoresponder-and-courier-maildrop-for-qmail-		<p><!-- \	\	@page { size: 8.5in 11in; margin: 0.79in } \	\	P { margin-bottom: 0.08in } \	--></p>\
<h2 style="margin-bottom: 0in">AUTORESPONDER:</h2>\
<p style="margin-bottom: 0in">autorespond-2.0.4.tar.gz is a part of QmailAdmin project. It is needed if you later want to compile with the auto-responding facility.</p>\
<p style="margin-bottom: 0in">The tarball is provided by the QMR package. This is the latest version on the net too (<a href="http://sourceforge.net/projects/qmailadmin/files/">http://sourceforge.net/projects/qmailadmin/files/</a>) . So let's follow it.</p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in"><!-- \	\	@page { margin: 0.79in } \	\	P { margin-bottom: 0.08in } \	-->Note:    \	Autorespond assumes that your qmail home is /var/qmail. If you qmail distribution is not in /var/qmail, then edit autorespond.c and change QMAIL_LOCATION to point to the correct directory.</p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">cd /downloads/qmailrocks</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">tar zxf <a href="http://sourceforge.net/projects/qmailadmin/files/autorespond/2.0.4/autorespond-2.0.4.tar.gz/download">autorespond-2.0.4.tar.gz</a></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">cd autorespond-2.0.4</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">make &amp;&amp; make install</span></p>\
<hr />\
<p> </p>\
<h2>COURIER MAILDROP:</h2>\
<p style="margin-bottom: 0in">In QMR guide, this step of installing MailDrop comes after vpopmail, Qmailadmin and Vqadmin . But I want to do it before vpopmail, so I can tell vpopmail to use it / enable it in vpopmail configuration.</p>\
<p style="margin-bottom: 0in">You do not need to download and install <em><span style="font-style: normal">maildrop if you already have</span></em> the <em>Courier</em> mail server installed. (Which we don't have). The standalone build of the <em>maildrop</em> mail filter can be used with other mail servers.</p>\
<p style="margin-bottom: 0in">This means that even if I don't user Courier-IMAP, I would still be able to use MailDrop from Courier. This is excellent option for my next project, in which I am going to use DoveCot, instead of CourierIMap.</p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">Why install it?</p>\
<p style="margin-bottom: 0in">We need it. If you remove it, it takes away <strong>reformime</strong> with it. And later, Qmail-scaner stops working. And here is from the Qmail-Scanner website:-</p>\
<blockquote>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"><em>Requirements</em></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"><em>* Netqmail 1.05 (or qmail-1.03 with patches)</em></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"><em>...</em></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"><em>...</em></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"><em>* daemontools-0.76+</em></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"><em>* reformime from Maildrop 1.3.8+</em></span></p>\
</blockquote>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">In-fact, your mail server will stop working properly if you remove it. Here is the output if I remove the maildrop RPM  from the system and run the qmail-scanner (doit) script :</p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">[root@www contrib]# ./test_installation.sh -doit</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">Sending standard test message - no viruses...</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">qmail-inject: fatal: qq temporary problem (#4.3.0)</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">Bad error. qmail-inject died</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">[root@www contrib]#</span></p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">Also, I get the following in the /var/log/maillog :</p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">May 10 14:11:44 www X-Qmail-Scanner-2.04: [www.wbitt.com121041070456210137] d_m: output spotted from /usr/bin/reformime  -x/var/spool/qscan/tmp/www.wbitt.com121041070456210137/ (sh: /usr/bin/reformime: No such file or directory</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">May 10 14:16:19 www X-Qmail-Scanner-2.04: [www.wbitt.com121041097956210184] d_m: output spotted from /usr/bin/reformime  -x/var/spool/qscan/tmp/www.wbitt.com121041097956210184/ (sh: /usr/bin/reformime: No such file or directory</span></p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">The FAQ # 9 and 10 on Qmail-Scanner site also explain the requirement of maildrop:</p>\
<p style="margin-bottom: 0in"><!-- \	\	@page { margin: 0.79in } \	\	P { margin-bottom: 0.08in } \	--></p>\
<ul>\
<li>\
<p style="margin-bottom: 0in"><strong>It says I must have reformime \	from the maildrop package! But I like procmail/[insert favorite MDA \	here]. Why do I have to.... </strong>You don't. reformime is needed for \	the extracting of MIME attachments by Qmail-Scanner - it doesn't \	have to be used by any other part of your system. Keep using \	procmail/whatever :-)</p>\
</li>\
</ul>\
<ul>\
<li>\
<p style="margin-bottom: 0in"><strong>It says I must have maildrop \	installed. Will I have to install .qmail files for all users...?.</strong> You don't. Maildrop isn't used at all - only the reformime program \	that comes with it is. Qmail-Scanner installs at a very low level \	and doesn't require any per-user configuration.</p>\
</li>\
</ul>\
<p> </p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">After re-installing maildrop, everything became fine again.</p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">The QMR site states:</p>\
<blockquote>\
<p style="margin-bottom: 0in">“<em>Part 7- Maildrop</em></p>\
<p style="margin-bottom: 0in"><em>Maildrop is a mail filtering agent which can be used to filter messages as they arrive on the server. You will probably notice, once this installation in complete, that you don't really use maildrop. However, it's a cool tool and it's worth having around if you ever decide to get crazy with filtering your incoming mail. .....”</em></p>\
</blockquote>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">This is not a true explanation. The true explanation, is specified in the beginning of this article.</p>\
<p style="margin-bottom: 0in"> </p>\
<h3 style="margin-bottom: 0in">Installation:</h3>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">Make sure you have pcre-devel and gcc-c++ installed on the system.</p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">yum -y install pcre-devel gcc-c++ gamin-devel gdbm-devel </span></p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">QMR provides  maildrop-1.6.3.tar.gz . Where as the latest is: maildrop-2.1.0.tar.bz2 . There is a strange problem with maildrop-2.1.0 and QmailScanner-2.06 .  For some stupid reason, QmailScanner-2.06 was treating maildrop-2.1.0 as maildrop-1.0+, declaring it BUGGY, OBSOLETE, and UN-USABLE. <span style="text-decoration: line-through;">So I for that instance I stuck  with the older version of maildrop, that is maildrop-1.6.3.</span> <strong>This is not the case any more. (Details further below).</strong></p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in"><!-- \	\	@page { margin: 0.79in } \	\	P { margin-bottom: 0.08in } \	--></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">wget http://sourceforge.net/projects/courier/files/maildrop/maildrop-2.1.0.tar.bz2</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">tar xjf maildrop-2.1.0.tar.bz2</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">cd maildrop-2.1.0</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"><br /></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">vi maildrop.spec</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">...</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">%configure --enable-maildirquota --enable-syslog=1 --enable-trusted-users='root mail daemon postmaster qmaild mmdf' --enable-maildrop-uid=root --enable-maildrop-gid=vchkpw</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">...</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">...</span></p>\
<p style="margin-bottom: 0in"> </p>\
<h3 style="margin-bottom: 0in">Create new tarball of the package:</h3>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">cd ..</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">mv maildrop-2.1.0.tar.bz2 maildrop-2.1.0.tar.bz2.orig</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">tar cjf maildrop-2.1.0.tar.bz2 maildrop-2.1.0</span></p>\
<p style="margin-bottom: 0in"> </p>\
<h3 style="margin-bottom: 0in">Build new RPM:</h3>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">rpmbuild -ta maildrop-2.1.0.tar.bz2</span></p>\
<p style="margin-bottom: 0in"> </p>\
<h3 style="margin-bottom: 0in">Stop Qmail, find your machine architecture (just to be sure) , erase old RPM, and install the new RPM:</h3>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"># qmailctl stop</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"><br /></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"># uname -i </span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">x86_64</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"><br /></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"># rpm -q maildrop</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">maildrop-2.0.4-1</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"> </span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"># rpm -e maildrop</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"><br /></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"># rpm -ivh /usr/src/redhat/RPMS/x86_64/maildrop-2.1.0-3.x86_64.rpm</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">Preparing...                ########################################### [100%]</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"> 1:maildrop               ########################################### [100%]</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"><br /></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"><br /></span></p>\
<h3 style="margin-bottom: 0in">Verify that it provides reforemime, the idea  behind the whole exercise:</h3>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"># rpm -ql maildrop | grep reformime</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">/usr/bin/reformime</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">/usr/share/doc/maildrop-2.1.0/reformime.html</span></p>\
<p style="margin-bottom: 0in"> </p>\
<p>That's all, maildrop is installed (latest version).</p>\
<p><strong>Note:</strong> Even if the author of Courier-IMAP drops vchkpw option from maildrop, it won't matter to us, because we are using only the reformime part of it for qmail-scanner.</p>\
<p> </p>\
<h2>Update:  I recently figured out the reason of Qmail-Scanner 2.06 barfing at maildrop, and fixed the entire issue. Below is the detail.</h2>\
<p style="margin-bottom: 0in"> </p>\
<h3 style="margin-bottom: 0in">The QS configure file (main culprit):</h3>\
<p style="margin-bottom: 0in">Looking inside the code of configure file provided with qmail-scanner-2.06 , I found the code:</p>\
<p style="margin-bottom: 0in"><!-- \	\	@page { margin: 0.79in } \	\	P { margin-bottom: 0.08in } \	--></p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">qmail-scanner-2.06]#  vi configure <br /></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">...  <br /></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">#Check version of maildrop to ensure it's not the buggy version</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">DD="`$MAILDROP_BINARY -v 2&gt;&amp;1|grep '^maildrop'|grep '1\\.0'`"</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">if [ "$DD" != "" ]; then</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"> cat&lt;&lt;EOF</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"> **************************</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"> You have a known bad version of maildrop (1.0). It is buggy and doesn't</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">work!</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">Please upgrade to a later release:</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">http://download.sourceforge.net/courier/</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"> **************************</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">EOF</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">fi</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">... </span></p>\
<br />\
<h3>The fix:</h3>\
<p style="margin-bottom: 0in">The grep check is checking for  existence of "1.0" in the text returned by the maildrop -v command. If it exists, it shows the error message and exits. Since co-incidently maildrop-2.1.0 also as "1.0" in it. This test fires up and the program exits. I have changed this code to the following so it works for every version of maildrop. And of-course in present time we have version higher than maildrop-1.3.8, which satisfies the requirements of qmail-scanner by default. Here is the updated code from qmail-scanner configure file, just for reference. This will be covered in more detail in Qmail-Scanner article:</p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in"><!-- \	\	@page { margin: 0.79in } \	\	P { margin-bottom: 0.08in } \	--></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">if [ "$MIME_UNPACKER" = "reformime" ]; then</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"> DD="`$MAILDROP_BINARY -v 2&gt;&amp;1 | grep '^maildrop'`"</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"> if [ "$DD" != "" ]; then</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"> echo "$DD found on this system."</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"> else</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"> echo "Maildrop was not found on this system. Please install and run Qmail-Scanner again."</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"> exit</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"> fi</span></p>\
<br />\
<p> </p>\
<br />\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">That's all .</p>\
<br />\
<p> </p>		1	11	0	34	2009-08-04 11:42:29	64		2010-12-26 11:33:11	63	0	0000-00-00 00:00:00	2009-08-04 11:42:29	0000-00-00 00:00:00			show_title=\
link_titles=\
show_intro=\
show_section=\
link_section=\
show_category=\
link_category=\
show_vote=\
show_author=\
show_create_date=\
show_modify_date=\
show_pdf_icon=\
show_print_icon=\
show_email_icon=\
language=\
keyref=\
readmore=	6	0	38	AutoResponder, Courier MailDrop, Qmail		0	6360	robots=\
author=Muhammad Kamran Azeem
