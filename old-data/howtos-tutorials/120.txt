120	Setting up PHPMyAdmin	setting-up-phpmyadmin		<p><!-- \	\	@page { margin: 0.79in } \	\	P { margin-bottom: 0.08in } \	--></p>\
<p style="margin-bottom: 0in">How to setup PHP My Admin:</p>\
<p style="margin-bottom: 0in">Download PHPMyAdmin from the home page:</p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">cd /root/ <br /></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">wget http://internap.dl.sourceforge.net/sourceforge/phpmyadmin/phpMyAdmin-2.11.9.5-english.zip</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">cp phpMyAdmin* /var/www</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">cd /var/www/</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">unzip phpMyAdmin-2.11.9.5-english.zip</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">ln -s phpMyAdmin-2.11.9.5-english phpmyadmin</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">chown apache:apache phpMyAdmin* -R</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">rm -f phpMyAdmin-2.11.9.5-english.zip</span></p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">Edit the create tables file , and enable the GRANT line with proper password:-</p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">vi /var/www/phpmyadmin/scripts/create_tables_mysql_4_1_2+.sql</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">...<br /></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">CREATE DATABASE IF NOT EXISTS `phpmyadmin`</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"> DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">USE phpmyadmin;</span></p>\
<p><span style="font-family: courier new,courier;">...</span></p>\
<p><span style="font-family: courier new,courier;">... <br /></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">--</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">-- Privileges</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">--</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">-- (activate this statement if necessary)</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">GRANT SELECT, INSERT, DELETE, UPDATE ON `phpmyadmin`.* TO 'pma'@localhost identified by 'phpsecret';</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">...</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">...</span></p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">Create PHPMyAdmin schema in MySQL server:-</p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">[root@www scripts]# mysql -u root -psecretpassword &lt; create_tables_mysql_4_1_2+.sql</span></p>\
<p> </p>\
<p> </p>\
Create Apache config file for PHPMyAmdin:-<br />\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">vi /etc/httpd/conf.d/phpmyadmin.conf</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">Alias /phpmyadmin /var/www/phpmyadmin</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">&lt;Location /phpmyadmin&gt;</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"> Order allow,deny</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;"> Allow from all</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">&lt;/Location&gt;</span></p>\
<p> </p>\
<p>Create web server writable folder config in phpMyAdmin toplevel directory:<br /><br /></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">cd /var/www/phpmyadmin</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">mkdir /var/www/phpmyadmin/config</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">mkdir /var/www/imports</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">mkdir /var/www/exports</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">chown apache:apache /var/www/phpmyadmin/ -R</span></p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in"> </p>\
<h2>Now run the phpmyadmin setup script, from the browser:</h2>\
<p style="margin-bottom: 0in"> </p>\
<br />\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">http://example.com/phpmyadmin/scripts/setup.php</span></p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">Once the setup starts, select secure connection.</p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">Features-Security : Force SSL</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">Upload Directory: /var/www/phpmyadmin/imports</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">Save Directory: /var/www/phpmyadmin/exports</span></p>\
<p style="margin-bottom: 0in">Configuration-Save: Save the config file.</p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">Configuration will be saved to file config/config.inc.php in phpMyAdmin top level directory, copy it to top level one and delete directory config to use it.</p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">[root@www phpmyadmin]# mv config/config.inc.php .</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">[root@www phpmyadmin]# rm -fr config</span></p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">Once completed. Open this URL and start using PHPMyAdmin:</p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">https://example.com/phpmyadmin/</span></p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">Each user can login using the name of his own DB user and password.</p>\
<p style="margin-bottom: 0in"> </p>\
<h2 style="margin-bottom: 0in">Manual steps:</h2>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">[root@www phpmyadmin]# mv config.sample.inc.php config.inc.php</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">vi config.inc.php</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">...</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">$cfg['blowfish_secret'] = 'secret'; /* YOU MUST FILL IN THIS FOR COOKIE AUTH! */</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">...</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">...</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">/* Authentication type */</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">$cfg['Servers'][$i]['auth_type'] = 'http';</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">/* Server parameters */</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">$cfg['Servers'][$i]['host'] = 'localhost';</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">$cfg['Servers'][$i]['connect_type'] = 'tcp';</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">$cfg['Servers'][$i]['compress'] = false;</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">/* Select mysqli if your server has it */</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">$cfg['Servers'][$i]['extension'] = 'mysql';</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">/* User for advanced features */</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">$cfg['Servers'][$i]['controluser'] = 'pma';</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">$cfg['Servers'][$i]['controlpass'] = 'phpsecret';</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">/* Advanced phpMyAdmin features */</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">$cfg['Servers'][$i]['pmadb'] = 'phpmyadmin';</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">$cfg['Servers'][$i]['bookmarktable'] = 'pma_bookmark';</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">$cfg['Servers'][$i]['relation'] = 'pma_relation';</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">$cfg['Servers'][$i]['table_info'] = 'pma_table_info';</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">$cfg['Servers'][$i]['table_coords'] = 'pma_table_coords';</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">$cfg['Servers'][$i]['pdf_pages'] = 'pma_pdf_pages';</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">$cfg['Servers'][$i]['column_info'] = 'pma_column_info';</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">$cfg['Servers'][$i]['history'] = 'pma_history';</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">$cfg['Servers'][$i]['designer_coords'] = 'pma_designer_coords';</span></p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">That should be all !</p>\
<p style="margin-bottom: 0in">Regards,</p>\
<p style="margin-bottom: 0in">Kamran</p>\
<p style="margin-bottom: 0in"> </p>		1	11	0	34	2009-08-03 19:57:05	64		2010-12-26 11:17:02	63	0	0000-00-00 00:00:00	2009-08-03 19:57:05	0000-00-00 00:00:00			show_title=\
link_titles=\
show_intro=\
show_section=\
link_section=\
show_category=\
link_category=\
show_vote=\
show_author=\
show_create_date=\
show_modify_date=\
show_pdf_icon=\
show_print_icon=\
show_email_icon=\
language=\
keyref=\
readmore=	2	0	41	phpmyadmin linux mysql kamran		0	5099	robots=\
author=Muhammad Kamran Azeem
