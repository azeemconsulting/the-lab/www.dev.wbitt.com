150	Tightening SPAM control on ISPConfig Server	tightening-spam-control-on-ispconfig-server		<meta http-equiv="CONTENT-TYPE" content="text/html; charset=utf-8" />\
<title></title>\
<meta name="GENERATOR" content="OpenOffice.org 3.1  (Unix)" />\
<!-- \	\	@page { margin: 0.79in } \	\	P { margin-bottom: 0.08in } -->\
<p style="margin-bottom: 0in;">Recently one of my clients shifted from Plesk to ISPConfig, and I was asked to setup ISPConfig control panel on it. We followed an ISPConfig Howto from howtoforge.com . The installation was (almost) a breeze. Migration from plesk to ISPConfig was quite painful. But anyway, we did it.</p>\
<p style="margin-bottom: 0in;">Later when the system went live and remained in production for more than a week, we noticed that there is a lot of spam coming in. The postfix mail server needed some additional armor. I wanted some important checks, such as helo, RBL and SPF. Below is how I added that extra level of protection.</p>\
<p style="margin-bottom: 0in;"> </p>\
<p style="margin-bottom: 0in;">First, I want to thank and acknowledge the authors of following web pages, which helped me in achieving this:</p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;"><br /></span></p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">http://www.wains.be/index.php/2006/04/04/postfix-spf/</span></p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">http://www.freesoftwaremagazine.com/articles/focus_spam_postfix?page=0%2C2</span></p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">http://www.howtoforge.com/postfix_spf</span></p>\
<p style="margin-bottom: 0in;"> </p>\
<p style="margin-bottom: 0in;">For SPF, I downloaded the postfix-SPF (module/plugin) from http://www.openspf.org/blobs/postfix-policyd-spf-perl-2.007.tar.gz , and installed it as following:</p>\
<p style="margin-bottom: 0in;"> </p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">cd /root/</span></p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">wget http://www.openspf.org/blobs/postfix-policyd-spf-perl-2.007.tar.gz</span></p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">tar xzf postfix-policyd-spf-perl-2.007.tar.gz</span></p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">cp postfix-policyd-spf-perl-2.007/postfix-policyd-spf-perl  /usr/libexec/postfix/</span></p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">chmod +x /usr/libexec/postfix/postfix-policyd-spf-perl</span></p>\
<p style="margin-bottom: 0in;"> </p>\
<p style="margin-bottom: 0in;">Then I had to add the following text (it is one /single long line) to bottom of /etc/postfix/master.cf :-</p>\
<p style="margin-bottom: 0in;"> </p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">vi /etc/postfix/master.cf</span></p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">...</span></p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">spfpolicy\	unix \	-\	n\	n\	-\	0\	spawn user=nobody argv=/usr/libexec/postfix/postfix-policyd-spf-perl</span></p>\
<p style="margin-bottom: 0in;"> </p>\
<p style="margin-bottom: 0in;">Notes:</p>\
<ul>\
<li>You can use Tabs instead of spaces in the line above. Refer to INSTALL file which comes with the tarball.</li>\
<li>The INSTALL file uses the word policy, instead of spfpolicy, as shown here. It does not matter. Whatever you choose to use, make sure that you use the same in master.cf and main.cf files.</li>\
</ul>\
<p style="margin-bottom: 0in;"> </p>\
<p style="margin-bottom: 0in;">I then edited my /etc/postfix/main.cf file and added the following text. The text below contains SPF checks, RBL checks, invalid helo checks, invalid host-name checks, etc.</p>\
<p style="margin-bottom: 0in;"> </p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">vi /etc/postfix/main.cf</span></p>\
<p style="margin-bottom: 0in;">. . .</p>\
<p style="margin-bottom: 0in;">(Change the following line:)</p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">smtpd_sender_restrictions = check_sender_access mysql:/etc/postfix/mysql-virtual_sender.cf</span></p>\
<p style="margin-bottom: 0in;"> </p>\
<p style="margin-bottom: 0in;">(Change to:)</p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">smtpd_sender_restrictions = \	reject_non_fqdn_sender, reject_unknown_sender_domain, check_sender_access mysql:/etc/postfix/mysql-virtual_sender.cf, permit</span></p>\
<p style="margin-bottom: 0in;"> </p>\
<p style="margin-bottom: 0in;">Note: The line above is single line.</p>\
<p style="margin-bottom: 0in;"> </p>\
<p style="margin-bottom: 0in;">(Then add the following text:)</p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">policy_time_limit = 3600smtpd_delay_reject = yes</span></p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">smtpd_helo_required = yes</span></p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">smtpd_helo_restrictions = permit_mynetworks, reject_non_fqdn_hostname, reject_invalid_hostname, permit</span></p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">smtpd_recipient_restrictions = reject_unauth_pipelining, reject_non_fqdn_recipient, reject_unknown_recipient_domain, permit_mynetworks, permit_sasl_authenticated, reject_unauth_destination, reject_rbl_client zen.spamhaus.org, reject_rbl_client bl.spamcop.net, check_policy_service unix:private/spfpolicy, permit</span></p>\
<p style="margin-bottom: 0in;"> </p>\
<p style="margin-bottom: 0in;">Note: smtpd_* lines shown above are individual long single lines. (Tip: smtpd_* till permit is one line.)</p>\
<p style="margin-bottom: 0in;"> </p>\
<p style="margin-bottom: 0in;">After you save this file, restart postfix service :</p>\
<p style="margin-bottom: 0in;"> </p>\
<p style="margin-bottom: 0in;"><span style="font-family: courier new,courier;">service postfix restart</span></p>\
<p style="margin-bottom: 0in;"> </p>\
<p style="margin-bottom: 0in;">That's all. I hope it to be helpful to those switching to ISPConfig, or Postfix server in particular. You can discuss anything related to this in http://forums.wbitt.com</p>\
<p style="margin-bottom: 0in;"> </p>		1	11	0	34	2010-01-05 16:33:32	64		2010-12-26 12:06:14	63	0	0000-00-00 00:00:00	2010-01-05 16:33:32	0000-00-00 00:00:00			show_title=\
link_titles=\
show_intro=\
show_section=\
link_section=\
show_category=\
link_category=\
show_vote=\
show_author=\
show_create_date=\
show_modify_date=\
show_pdf_icon=\
show_print_icon=\
show_email_icon=\
language=\
keyref=\
readmore=	12	0	27	postfix, spf, anti spam, rbl, helo		0	11969	robots=\
author=Muhammad Kamran Azeem
