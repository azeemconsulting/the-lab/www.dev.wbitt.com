132	Setting up an RPM build environment for an ordinary user	setting-up-an-rpm-build-environment-for-an-ordinary-user		<!-- \	\	@page { margin: 0.79in } \	\	P { margin-bottom: 0.08in } \	-->\
<h1 style="margin-bottom: 0in">Setting up an RPM build environment in the home directory of a non-root user:</h1>\
<p style="margin-bottom: 0in">Suppose we need to build some RPMs , not as root, but as a normal user, e.g. kamran. Example of such software would be Courier-IMAP server. Kamran's home directory is /home/kamran . We would need to do the following simple steps:</p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">mkdir $HOME/rpm/{SOURCES,SPECS,BUILD,SRPMS,RPMS} -p</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">mkdir $HOME/rpm/RPMS/{i386,noarch,x86_64}</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">echo "%_topdir $HOME/rpm" &gt;&gt; $HOME/.rpmmacros</span></p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in">That's all. For an example software like Courier-IMAP build, you would now be using the same rpmbuild  command, with the following results:</p>\
<p style="margin-bottom: 0in"><!-- \	\	@page { margin: 0.79in } \	\	P { margin-bottom: 0.08in } \	--></p>\
<p style="margin-bottom: 0in"> </p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">[kamran@server ~]$ rpmbuild -ta courier-imap-4.5.0.tar.bz2</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">... <br /></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">... <br /></span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">Wrote: /home/kamran/rpm/SRPMS/courier-imap-4.5.0-3.src.rpm</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">Wrote: /home/kamran/rpm/RPMS/x86_64/courier-imap-4.5.0-3.x86_64.rpm</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">Wrote: /home/kamran/rpm/RPMS/x86_64/courier-imap-debuginfo-4.5.0-3.x86_64.rpm</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">Executing(%clean): /bin/sh -e /var/tmp/rpm-tmp.72263</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">+ umask 022</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">+ cd /home/kamran/rpm/BUILD</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">+ cd courier-imap-4.5.0</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">+ rm -rf /var/tmp/courier-imap-4.5.0-3-buildroot</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">+ exit 0</span></p>\
<p style="margin-bottom: 0in"><span style="font-family: courier new,courier;">[kamran@server ~]$</span></p>\
<br />\
<p> </p>\
<p style="margin-bottom: 0in">Congratulations!</p>		1	11	0	34	2009-08-16 10:36:23	64		2010-12-26 11:49:20	63	0	0000-00-00 00:00:00	2009-08-16 10:36:23	0000-00-00 00:00:00			show_title=\
link_titles=\
show_intro=\
show_section=\
link_section=\
show_category=\
link_category=\
show_vote=\
show_author=\
show_create_date=\
show_modify_date=\
show_pdf_icon=\
show_print_icon=\
show_email_icon=\
language=\
keyref=\
readmore=	2	0	34	rpm build environment for non-root ordinary users		0	4807	robots=\
author=Muhammad Kamran Azeem
