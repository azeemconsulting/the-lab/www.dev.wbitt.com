89	YUM local repository	yum-local-repository		Setting up local yum repository:-<br /><br />Assuming you have your Linux distribution DVD / CDs copied into a directory named "/data/rhel_5.2dvd", then you would setup a yum repository file as shown below, to help you eliminate the need to copy/install individual packages all the time , and use the power of yum instead:-<br /><br /><br />[root@headnode Server]# cat /etc/yum.repos.d/localmedia.repo<br />[rhel-localmedia]<br />name=RedHat Enterprise Linux $releasever - $basearch<br />baseurl=file:///data/rhel_5.2dvd/Server/<br />enabled=1<br />gpgcheck=1<br />gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release<br />[root@headnode Server]#  <br /><br />That is all!		1	11	0	34	2009-03-22 08:04:07	64		2010-12-25 19:46:07	74	0	0000-00-00 00:00:00	2009-03-22 08:04:07	0000-00-00 00:00:00			show_title=\
link_titles=\
show_intro=\
show_section=\
link_section=\
show_category=\
link_category=\
show_vote=\
show_author=\
show_create_date=\
show_modify_date=\
show_pdf_icon=\
show_print_icon=\
show_email_icon=\
language=\
keyref=\
readmore=	3	0	52	YUM local repository		0	4931	robots=\
author=
